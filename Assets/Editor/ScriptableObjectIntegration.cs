﻿using UnityEngine;
using UnityEditor;

static class ScriptableObjectIntegration
{

    [MenuItem("Tools/Create/ScriptableGameModel")]
    public static void CreateYourScriptableObject()
    {
        ScriptableObjectUtility.CreateAsset<ScriptableGameModel>();
    }

}