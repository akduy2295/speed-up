using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

public class BuildScriptEditor
{
    static string[] SCENES = FindEnabledEditorScenes();
    private const string TimeFormat = "yyyy-MM-dd-hh-mm-ss";
    private static string APP_NAME = PlayerSettings.productName;
    private static string COMPANY_NAME = PlayerSettings.companyName;
    private static string PARRENT_DIR = @"D:\Projects\TestJenskin\TestBuild\";
    private static string PC_DIR = @"PC\";
    private static string ANDROID_DIR = @"Android\";
    private static string datetime = "";

    /*[MenuItem("Custom/CI/Build Windown")]
    static void BuildPC()
    {
        string target_dir = APP_NAME + ".exe";
        datetime = GetDateTime();
        string folderName = APP_NAME + "_" + datetime + @"\";
        GenericBuild(SCENES, PARRENT_DIR + PC_DIR + folderName + target_dir, BuildTarget.StandaloneWindows, BuildOptions.None);

    }

    [MenuItem("Custom/CI/Build Android")]
    static void BuildAndroid()
    {
        string target_dir = APP_NAME + ".apk";
        PlayerSettings.applicationIdentifier = "com." + COMPANY_NAME.ToLower() + "." + APP_NAME.ToLower();
        datetime = GetDateTime();
        string folderName = APP_NAME + "_" + datetime + @"\";
        GenericBuild(SCENES, PARRENT_DIR + ANDROID_DIR + folderName + target_dir, BuildTarget.Android, BuildOptions.None);
    }*/

    [MenuItem("Custom/PCTableSetting")]
    static void PCTableSetting()
    {
        PlayerSettings.defaultIsFullScreen = false;
        PlayerSettings.resizableWindow = true;
        PlayerSettings.runInBackground = true;
        PlayerSettings.defaultScreenWidth = 1024;
        PlayerSettings.defaultScreenHeight = 768;
    }

    private static string GetDateTime()
    {
        return DateTime.Now.ToString(TimeFormat);
    }

    private static string[] FindEnabledEditorScenes()
    {
        List<string> EditorScenes = new List<string>();
        foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
        {
            if (!scene.enabled) continue;
            EditorScenes.Add(scene.path);
        }
        return EditorScenes.ToArray();
    }

    static void GenericBuild(string[] scenes, string target_dir, BuildTarget build_target, BuildOptions build_options)
    {
        Debug.Log(target_dir);
        EditorUserBuildSettings.SwitchActiveBuildTarget(build_target);
        if (build_target == BuildTarget.Android)
        {
            EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;
        }
        BuildPipeline.BuildPlayer(scenes, target_dir, build_target, build_options);
       
    }

    /// <summary>
    /// Call from Unity cloud build
    /// </summary>
    static void RunConfig()
    {
        PlayerSettings.SplashScreen.show = false;
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android,"");
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, "");
        PlayerSettings.Android.minSdkVersion = AndroidSdkVersions.AndroidApiLevel23;
        PlayerSettings.Android.targetSdkVersion = AndroidSdkVersions.AndroidApiLevelAuto;
#if UNITY_ANDROID
        PlayerSettings.Android.forceSDCardPermission = true;
#endif
#if UNITY_STANDALONE_WIN
        PlayerSettings.displayResolutionDialog = ResolutionDialogSetting.Disabled;
#endif
    }
}
