﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.EyeClickSDK.Scripts.interactions
{
    public class AndroidEngine
    {
#if UNITY_ANDROID 
        private AndroidJavaClass _androidEngineInterface;

        public static string GetExternalResourcesFolderPath()
        {
            AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject activity = jc.GetStatic<AndroidJavaObject>("currentActivity");
            return new AndroidJavaClass("com.eyeclick.engineunityplugin.ExternalStorage").CallStatic<string>("getExternalResourceFolderPath", activity);
        }

        public void EyeclickGiInit(Interaction.EngineType engineType)
        {
            _androidEngineInterface = new AndroidJavaClass("com.eyeclick.engineunityplugin.EnginePlugin");
            AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");

            var engineTypeString = ParseEngineType(engineType);
            //Debug.Log("Start call initEngine " + engineType);
            _androidEngineInterface.CallStatic("initEngine", jo, engineTypeString);
            Debug.Log("engine type is " + engineType);
        }

        private string ParseEngineType(Interaction.EngineType engineType)
        {
            switch (engineType)
            {
                case Interaction.EngineType.FootEdge:
                    return "foot_edge";
                case Interaction.EngineType.PointingFinger:
                    return "pointing_finger";
                case Interaction.EngineType.Random:
                    return "random";
                case Interaction.EngineType.Silhouette:
                    return "silhouette";
                case Interaction.EngineType.hand_silhouette:
                    return "hand_silhouette";
                default:
                    return "foor_edge";
            }
        }

        public void SendKeys(List<string> keys)
        {
            _androidEngineInterface.CallStatic("sendKeys", String.Join(";", keys.ToArray()));
        }

        public List<Interaction.Point> PopMesssage()
        {
            //Debug.Log("Start PopMesssage");
            string keyPointsJson = _androidEngineInterface.CallStatic<string>("pullKeyPoints");
            KeyPointsList keyPointsList = JsonUtility.FromJson<KeyPointsList>(keyPointsJson);
            if (keyPointsList.keyPointsList.Count == 0)
                return new List<Interaction.Point>();

            var points = new List<Interaction.Point>();
            foreach (var kp in keyPointsList.keyPointsList)
            {                
                foreach (var p in kp.points)
                {
                    var point = new Interaction.Point
                    {
                        x = p.x,
                        y = p.y,
                        id = p.id
                    };
                    points.Add(point);
                }
            }
            //Debug.Log("End PopMesssage");
            return points;
        }

        public void OnGamePaused()
        {
            _androidEngineInterface.CallStatic("onGamePaused");
        }

        public void OnGameResumed()
        {
            _androidEngineInterface.CallStatic("onGameResumed");
        }

        [Serializable]
        private class KeyPointsList
        {
            public List<KeyPoints> keyPointsList;
        }

        [Serializable]
        private class KeyPoints
        {
            public List<Point> points;
        }

        [Serializable]
        private class Point
        {
            public int id;
            public int x;
            public int y;
        }
#endif
    }
}