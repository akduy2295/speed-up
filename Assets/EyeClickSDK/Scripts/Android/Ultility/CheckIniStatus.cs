﻿using System.Collections;
using Assets.EyeClickSDK.Scripts.interactions;
using UnityEngine;
using UnityEngine.UI;

public class CheckIniStatus : UltilityAbstract
{
    public Text IniStatusTxt;

    private bool isShow = false;
    private string currentStatusIni = "";

    private string format = "Ini: {0}";

	// Use this for initialization
	void Start ()
	{
	    
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (isShow)
        {
            IniStatusTxt.text = string.Format(format, currentStatusIni);
        }
        
    }

   
    public override void OnShow()
    {
        isShow = true;
        currentStatusIni = ErrorCollector.GetLastMessageByType(ErrorMessageType.Error_INI).Message;
    }

    public override void OnHide()
    {
        isShow = false;
        IniStatusTxt.text = "";
    }
}
