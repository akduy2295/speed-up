﻿using System.Collections;
using System.Collections.Generic;

public enum ErrorMessageType
{
    Error_INI,
}

public class MessageItem
{
    public ErrorMessageType Type;
    public string Message;

    public MessageItem()
    {

    }
    public MessageItem(ErrorMessageType type)
    {
        Type = type;
        Message = "No Error";
    }
}

public static class ErrorCollector
{
    private static List<MessageItem> errorMessage = new List<MessageItem>();

    public static void AddMessage(ErrorMessageType type, string message)
    {
        MessageItem item = new MessageItem();
        item.Type = type;
        item.Message = message;
        errorMessage.Add(item);
    }


    public static MessageItem GetLastMessageByType(ErrorMessageType type)
    {
        for (int i = errorMessage.Count-1; i >=0; i--)
        {
            var item = errorMessage[i];
            if (item.Type == type)
            {
                return item;
            }
        }
        return new MessageItem(type);
    }

    public static List<MessageItem> GetAllMessageByType(ErrorMessageType type)
    {
        List<MessageItem> messages = new List<MessageItem>();
        for (int i = 0; i < errorMessage.Count; i++)
        {
            var item = errorMessage[i];
            if (item.Type == type)
            {
                messages.Add(item);
            }
        }
        return messages;
    }

    public static bool HaveErrorByType(ErrorMessageType type)
    {
        for (int i = 0; i < errorMessage.Count; i++)
        {
            var item = errorMessage[i];
            if (item.Type == type)
            {
                return true;
            }
        }
        return false;
    }
}
