﻿using System.Collections;
using System.Collections.Generic;
using Assets.EyeClickSDK.Scripts.interactions;
using UnityEngine;
using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using UnityEngine.Profiling;
using UnityEngine.UI;

public class LogMessageInfo
{
    public string Condition;
    public string StackTrace;
}

public class LogManager : Singleton<LogManager>
{
    private const string TimeFormat = "yyyy-MM-dd";
    private string filenameFormat = "{0}_{1}_{2}";
    private string memoryFormat = "*** LOW MEMORY ***" +'\n' +
                                  "Total mem alloc: {0} MB" + '\n' +
                                  "Total reserved mem: {1} MB" + '\n' +
                                  "Reserved mem but not allocated: {2} MB" + '\n' +
                                  "System mem: {3} MB" + '\n' +
                                  "**************************";
    private int exceptionLimit = 10;    
    private int currentLog = 0;
    private int logLimit = 10;
    private string version = "";
    private string logPath = "logs";    

    //List<Texture2D> _textures = new List<Texture2D>();
    private ReadIni iniAndroid;

    private bool enableLog;

    void Awake ()
	{
	    Init();
    }  

    private void Init()
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        string externalResourcesFolderPath = ReadIni.GetPath();
        var splitString = externalResourcesFolderPath.Split('/');
        var gameName = splitString[splitString.Length - 1];
        iniAndroid = new ReadIni();
        enableLog = iniAndroid.readBool("Log", "EnableLog", false);
        logPath = iniAndroid.readString("Log", "LogPath", "logs");
        logLimit = iniAndroid.readInt("Log", "LogLimit", 10);
        exceptionLimit = iniAndroid.readInt("Log", "ExceptionLimit", 10);
        Debug.Log("******* Log Enable: " + enableLog);
        if (enableLog)
        {
            Application.logMessageReceived += OnLogRecieve;
            Application.lowMemory += OnLowMemory;
        }
#else
        Application.logMessageReceived += OnLogRecieve;
        Application.lowMemory += OnLowMemory;
        Debug.Log("******* Log Enable: " + enableLog);
#endif
        version = Assembly.GetExecutingAssembly().GetName().Version.ToString();

        CreateGameLogFolder();

        LoadLogFile(CreateGameFolderPath());
    }

    void OnLowMemory()
    {
        LogMessageInfo messageInfo = new LogMessageInfo();
        messageInfo.Condition = string.Format(memoryFormat, Profiler.GetTotalAllocatedMemoryLong() / 1000000*1.0f,
            Profiler.GetTotalReservedMemoryLong() / 1000000 * 1.0f,
            Profiler.GetTotalUnusedReservedMemoryLong() / 1000000 * 1.0f,
            SystemInfo.systemMemorySize);
        messageInfo.StackTrace = "";
        SaveLogFile(messageInfo);
    }

    void OnLogRecieve(string condition, string stackTrace, LogType type)
    {
        if (currentLog > exceptionLimit)
        {
            return;
        }
        if (type == LogType.Exception)
        {
            LogMessageInfo messageInfo = new LogMessageInfo();
            messageInfo.Condition = condition;
            messageInfo.StackTrace = stackTrace;
            SaveLogFile(messageInfo);
        }
    }

    private void LoadLogFile(string path)
    {
        List<string> files = GetAllFilesFromFolderPath(path);
        if (files.Count > logLimit) 
        {
            files = SortLogByTime(files);
            for (int i = 0; i < files.Count; i++)
            {                
                if(i >= logLimit - 1)
                {
                    File.Delete(files[i]);
                }
            }
        }        
    }

    private void SaveLogFile(LogMessageInfo logInfo)
    {
        string[] data = new []{ logInfo.Condition, logInfo .StackTrace};        
        var fullpath = CreateFullFilenamePath();
        StreamWriter sw;
        sw = File.AppendText(fullpath);
        sw.Write("***"+DateTime.Now+ "***"+'\n');
        for (int i = 0; i < data.Length; i++)
        {
            sw.Write(data[i]+'\n');
        }
        sw.Close();
        currentLog++;
    }

    private string CreateGameFolderPath()
    {
        string externalResourcesFolderPath = "";
#if !UNITY_EDITOR && UNITY_ANDROID
        externalResourcesFolderPath = AndroidEngine.GetExternalResourcesFolderPath();
        
#else
        externalResourcesFolderPath = Application.persistentDataPath;
#endif
        //var splitString = externalResourcesFolderPath.Split('/');
        //var gameName = splitString[splitString.Length - 1];
        //var newPath = "";
        //for (int i = 0; i < splitString.Length; i++)
        //{
        //    string part = splitString[i];
        //    newPath += splitString[i] + "/";
        //    if (part == "Eyeclick")
        //    {
        //        newPath += "games/" + gameName + "/" + logPath + "/";
        //        break;
        //    }
        //}       
        string newPath = externalResourcesFolderPath + "/" + logPath + "/";
        return newPath;
    }

    private string CreateFullFilenamePath()
    {
        string externalResourcesFolderPath = "";
#if !UNITY_EDITOR && UNITY_ANDROID
        externalResourcesFolderPath = AndroidEngine.GetExternalResourcesFolderPath();
        
#else
        externalResourcesFolderPath = Application.persistentDataPath;
#endif
        Debug.Log("***ResourcesFolderPath: " + externalResourcesFolderPath);
        var splitString = externalResourcesFolderPath.Split('/');
        var gameName = splitString[splitString.Length - 1];
        var newPath = externalResourcesFolderPath + "/" + logPath + "/"; 
        //for (int i = 0; i < splitString.Length; i++)
        //{
        //    string part = splitString[i];
        //    newPath += splitString[i] + "/";
        //    if (part == "Eyeclick")
        //    {
        //        newPath += "logs/" + gameName + "/";
        //        break;
        //    }
        //}
        Debug.Log("***Log Path: " + newPath);
        gameName = string.Format(filenameFormat, gameName,version, DateTime.Now.ToString(TimeFormat));
        Debug.Log("***gameName: " + gameName);
        newPath += gameName + ".txt";
        Debug.Log("***Newpath: " + newPath);
        return newPath;
    }

    private List<string> GetAllFilesFromFolderPath(string path)
    {        
        List<string> list = new List<string>();
        const string type = ".txt";      
        DirectoryInfo d = new DirectoryInfo(path);
        FileInfo[] files = d.GetFiles(@"*" + type);        
        for (int i = 0; i < files.Length; i ++)
        {
            FileInfo file = files[i];
            if (file.FullName.Contains(type) && !file.FullName.Contains(".meta"))
            {
                list.Add(file.FullName);
            }
        }
        return list;
    }

    private List<string> SortLogByTime(List<string> logPaths)
    {
        for (int i = 0; i < logPaths.Count; i++)
        {
            for (int j = i; j < logPaths.Count; j++)
            {
                TimeSpan t1 = GetLogTimeSpan(logPaths[i]);
                TimeSpan t2 = GetLogTimeSpan(logPaths[j]);
                if (t1.TotalSeconds > t2.TotalSeconds)
                {
                    string tempPath = logPaths[i];
                    logPaths[i] = logPaths[j];
                    logPaths[j] = tempPath;
                }
            }
        }
        return logPaths;
    }

    private TimeSpan GetLogTimeSpan(string path)
    {
        string filename = path.Substring(path.LastIndexOf("/", StringComparison.Ordinal) + 1);
        string dateTimeStr = filename.Substring(filename.LastIndexOf("_", StringComparison.Ordinal) + 1);
        // Remove extension
        dateTimeStr = dateTimeStr.Substring(0, dateTimeStr.Length - 4);
        DateTime dateTime = DateTime.ParseExact(dateTimeStr, TimeFormat, null);
        TimeSpan diff = DateTime.Now.Subtract(dateTime);
        return diff;
    }

    private bool IsFileTooOld(string path)
    {
        string filename = path.Substring(path.LastIndexOf("/", StringComparison.Ordinal) + 1);        
        string dateTimeStr = filename.Substring(filename.LastIndexOf("_", StringComparison.Ordinal) + 1);
        // Remove extension
        dateTimeStr = dateTimeStr.Substring(0, dateTimeStr.Length - 4);        
        DateTime dateTime = DateTime.ParseExact(dateTimeStr, TimeFormat, null);
        TimeSpan diff = DateTime.Now.Subtract(dateTime);        
        return diff.TotalDays > 10;        
    }

    private void CreateGameLogFolder()
    {
        string newPath = CreateGameFolderPath();
        if (!Directory.Exists(newPath))
        {
            Directory.CreateDirectory(newPath);
        }
    }
}