﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UltilityManager : MonoBehaviour
{
    public static UltilityManager instance;

    public GameObject DetailPanel;
    public RectTransform LogParent;
    public GameObject LogItemPrefab;

    public Image test = null;

    private bool showDebugMode = false;
    private bool showDetailLog = false;

    private List<LogItem> logDetailItems = new List<LogItem>();
    private Coroutine currentCoroutine = null;
    public bool ShowDetailLog
    {
        get { return showDetailLog; }
        set
        {
            showDetailLog = value;
            if (showDetailLog)
            {
                DetailPanel.SetActive(true);
                currentCoroutine = StartCoroutine(CreateLogItemINI());
            }
            else
            {
                DetailPanel.SetActive(false);
            }
        }
    }

    public bool ShowDebugMode
    {
        get { return showDebugMode; }
        set
        {
            showDebugMode = value;
            if (showDebugMode)
            {
                for (int i = 0; i < Ultilities.Count; i++)
                {
                    Ultilities[i].OnShow();
                }
            }
            else
            {
                for (int i = 0; i < Ultilities.Count; i++)
                {
                    Ultilities[i].OnHide();
                }
            }
        }
    }
    public List<UltilityAbstract> Ultilities = new List<UltilityAbstract>();

    void Awake()
    {
        instance = this;
    }

    // Use this for initialization
	void Start ()
	{
	    ShowDebugMode = false;
	}
	
	// Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            Debug.Log("Press D");
            ShowDebugMode = !ShowDebugMode;
        }
        /*if (Input.GetKeyDown(KeyCode.L))
        {
            ShowDetailLog = !ShowDetailLog;
        }*/
    }

    public IEnumerator CreateLogItemINI()
    {
        List<MessageItem> listMessageINI = ErrorCollector.GetAllMessageByType(ErrorMessageType.Error_INI);
        for (int i = 0; i < listMessageINI.Count; i++)
        {
            var messageItem = listMessageINI[i];
            var logItemGo = Instantiate(LogItemPrefab);
            logItemGo.transform.SetParent(LogParent);
            var logItem = logItemGo.GetComponent<LogItem>();
            logItem.MessageTxt.text = messageItem.Type + ": " + messageItem.Message;
            logDetailItems.Add(logItem);
            yield return new WaitForEndOfFrame();
        }
        
    }


    public void DestroyAllLogDetail()
    {
        if (currentCoroutine != null)
        {
            StopCoroutine(currentCoroutine);
            currentCoroutine = null;
        }
        for (int i = logDetailItems.Count-1; i >=0; i--)
        {
            Destroy(logDetailItems[i].gameObject);
        }
        logDetailItems.Clear();
    }

}
