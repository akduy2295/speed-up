using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using Assets.EyeClickSDK.Scripts.interactions;

public class Branding : MonoBehaviour
{
	void Awake()
	{
	    string TextureLocation = "";
	    TextureLocation = ReadIni.GetPath() + @"/Branding.png";
/*#if UNITY_EDITOR
        Debug.Log("Editor");
        //TextureLocation = Application.dataPath + "/Textures" + "/" + "Branding.png";
	    DirectoryInfo dir = new DirectoryInfo(".");
	    string dirName = dir.Name;
        TextureLocation = Directory.GetCurrentDirectory() + @"/" + dirName + @"/" + "Branding.png";
#endif
#if UNITY_ANDROID && !UNITY_EDITOR
        Debug.Log("Build Android");
        string externalResourcesFolderPath = AndroidEngine.GetExternalResourcesFolderPath();
		TextureLocation = externalResourcesFolderPath + "/" + "Branding.png";
#endif
#if UNITY_STANDALONE_WIN && !UNITY_EDITOR
	    Debug.Log("Build PC");
        DirectoryInfo dir = new DirectoryInfo(".");
        string dirName = dir.Name;
        TextureLocation = Directory.GetCurrentDirectory() + @"/" + dirName + @"/" + "Branding.png";
#endif*/
        if (File.Exists(TextureLocation))
        {
            LoadBranding(TextureLocation);
        }
		else
		{
			Debug.Log("ERROR NOT FIND FILE WITH PATH " + TextureLocation);
		}

	}

    private void LoadBranding(string textureLocation)
    {
        Texture2D tex = new Texture2D(4, 4, TextureFormat.RGBA32, false);
        tex.LoadImage(File.ReadAllBytes(textureLocation));
        Sprite tmp = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
        Image image = GetComponent<Image>();
        image.sprite = tmp;
        image.enabled = true;
    }

}