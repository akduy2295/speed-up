﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckDefineSymbol : MonoBehaviour
{

    public Text Txt;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	    string text = "";
#if UNITY_STANDALONE_WIN
        text += "UNITY_STANDALONE_WIN";
#endif
#if UNITY_ANDROID
        text += "UNITY_ANDROID";
#endif
	    Txt.text = text;
	}
}
