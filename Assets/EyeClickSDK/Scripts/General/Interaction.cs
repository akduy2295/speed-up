﻿using System;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
using System.Runtime.InteropServices;
using mgCommon;
#endif

namespace Assets.EyeClickSDK.Scripts.interactions
{
    public class Interaction : Singleton<Interaction>
    {
#if UNITY_ANDROID
        private const int LeftMouseButton = 0;
        public enum EngineType
        {
            FootEdge,
            Silhouette,
            Random,
            PointingFinger,
            hand_silhouette,
        }

        public EngineType _EngineType;

        public struct Data
        {
            public RaycastHit hit;
            public Vector2 Position;
            public float id;
        }

        public List<Data> TouchList = new List<Data>();

        private readonly AndroidEngine _engine = new AndroidEngine();

        void Start()
        {
            Application.targetFrameRate = 60;
#if TABLE
            _EngineType = EngineType.PointingFinger;
#elif WALL
            Screen.SetResolution(1280,800,true);
             _EngineType = EngineType.hand_silhouette;
#else
            Screen.SetResolution(1280,800,true);
            _EngineType = EngineType.Silhouette;
#endif
            _engine.EyeclickGiInit(_EngineType);
        }

        void Update()
        {
            TouchList.Clear();
#if !UNITY_EDITOR
            SendKeysToEngine();
            ReadFromEngine();
#endif
            ReadMouseClicks();
        }
        private void Monkey()
        {
            for (int i = 0; i < 20; i++)
            {
                TouchList.Add(BuildTouchData(RandomPoint(), 0));
            }
        }
        private Vector2 RandomPoint()
        {
            return new Vector2(UnityEngine.Random.Range(0, Screen.width), UnityEngine.Random.Range(0, Screen.height));
        }

        void OnApplicationPause(bool isPaused)
        {
            if (isPaused)
            {
                _engine.OnGamePaused();
            }
            else
            {
                _engine.OnGameResumed();
            }
        }

        private void SendKeysToEngine()
        {
            if (!Input.anyKeyDown)
            {
                return;
            }
            var downKeys = readKeys();
            _engine.SendKeys(downKeys);
        }

        private List<string> readKeys()
        {
            var downKeys = new List<string>();
            foreach (KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKeyDown(kcode))
                    downKeys.Add(kcode.ToString());
            }
            return downKeys;
        }

        private void ReadFromEngine()
        {
#if TABLE
            Vector2 offset = WindowMover.TopLeft;
            foreach (var p in _engine.PopMesssage())
            {
                TouchList.Add(BuildTouchData(new Vector2(p.x + offset.x, Screen.height - p.y + offset.y), p.id));
            }
#else
            foreach (var p in _engine.PopMesssage())
            {
                TouchList.Add(BuildTouchData(new Vector2(p.x, Screen.height - p.y), p.id));
            }
       
#endif
        }
        private void ReadMouseClicks()
        {
            if (Input.GetMouseButton(LeftMouseButton))
            {
                TouchList.Add(BuildTouchData(new Vector2(Input.mousePosition.x, Input.mousePosition.y), 0));
            }
        }

        private Data BuildTouchData(Vector2 dataPosition, int id)
        {
            var ray = Camera.main.ScreenPointToRay(dataPosition);
            RaycastHit hit;
            var data = new Data();
            if (Physics.Raycast(ray, out hit, 1000))
            {
                data.hit = hit;
                data.id = id;
            }

            data.Position = dataPosition;
            return data;
        }

        public class Point
        {
            public int id;
            public int x;
            public int y;
        }
#elif UNITY_STANDALONE_WIN
        int ScreenWidth;
        int ScreenHeight;
   
	    public bool EnableMonkey = false;
	    public int NumPlayer = 100;

	    private float width;
	    private float height;

        private void GetScreenRes()
        {
		
            ScreenWidth = new IniFile(@"C:\Program Files (x86)\EyeClick\Applications\global\applications.ini").readInt("Application", "Width", 1024);
            ScreenHeight = new IniFile(@"C:\Program Files (x86)\EyeClick\Applications\global\applications.ini").readInt("Application", "Height", 768);

        }
        
        byte[] sil = new byte[32 * 24];

        public static Vector2[] _sils = new Vector2[32 * 24];

        /// <summary>
        /// Pop Message
        /// </summary>
        [DllImport("EyeclickGI.dll", EntryPoint = "?EyeclickGI_init@@YAXXZ")]
        static extern void EyeclickGI_init();

        [DllImport("EyeclickGI.dll", EntryPoint = "?PopMesssage@@YAHPAUEYECLICK_MSG@@@Z")]
        static extern int PopMesssage(ref EyeClickMsg Msg);

        [DllImport("user32.dll", EntryPoint = "GetWindowRect")]
        public static extern bool GetWindowRect(IntPtr hwnd, ref Rect rectangle);

        [DllImport("user32.dll", EntryPoint = "FindWindow")]
        public static extern IntPtr FindWindow(System.String className, System.String windowName);

        [DllImport("LightTouchClient.dll")]
        [return: MarshalAs(UnmanagedType.I1)]

        public static extern bool getSilhoutteData(int mode, byte[] silData);

        private static EyeClickMsg _EyeClickMsg = new EyeClickMsg();

        public struct EyeClickMsg
        {
            public float fMessageID;
            public float fData1;
            public float fData2;
            public float fData3;
            public float fData4;
        }

        public enum EngineDataType
        {
            LightTouchClient,
            PopMessage
        }

        public EngineDataType _engineDataType = EngineDataType.LightTouchClient;

        public struct Data
        {
            public RaycastHit hit;
            public Vector2 Position;
            public float id;
        }

        public struct PlayerBodyLocation
        {
            public float PositionX;
            public float PositionY;
            public float Length;
            public float Height;
            public float TrackID;
        }


        [DllImport("EyeclickGI", EntryPoint = "?RequestEventNotification@@YAHM@Z")]
        static extern int RequestEventNotification(float fEventID);
        // A list to contain all the touchs
        public List<Data> TouchList = new List<Data>();
        public List<PlayerBodyLocation> PlayerDataList = new List<PlayerBodyLocation>();
        //public List<LightTouchClientWithId.TouchPoint> LightTouchList = new List<LightTouchClientWithId.TouchPoint>();
        void Start()
        {
            width = Screen.width;
            height = Screen.height;
            GetScreenRes();
#if TABLE
            _engineDataType = EngineDataType.PopMessage;
            GetWindowPos();
#else
            _engineDataType = EngineDataType.LightTouchClient;
#endif
            EyeclickGI_init();
            RequestEventNotification(11.71062f);
            RequestEventNotification(19.71062f);
            
        }

#if TABLE
        [DllImport("user32.dll")]
        static extern IntPtr GetActiveWindow();
        void GetWindowPos()
        {
            Rect rect = new Rect();
            while (GetWindowRect(FindWindow(null, Application.productName), ref rect) == false)
            {

            }

            UnityEngine.Debug.Log("ScreenWidth:" + ScreenWidth);
            UnityEngine.Debug.Log("ScreenHeight:" + ScreenHeight);
            WindowMover.TopLeft.x = float.Parse(rect.Left.ToString()) / float.Parse(ScreenWidth.ToString());
            WindowMover.TopLeft.y = float.Parse(rect.Top.ToString()) / float.Parse(ScreenHeight.ToString());
            UnityEngine.Debug.Log("Window position left rect:" + rect.Left);
            UnityEngine.Debug.Log("Window position is:" + WindowMover.TopLeft.x + " " + WindowMover.TopLeft.y);
        }

        public struct Rect
        {
            public int Left { get; set; }
            public int Top { get; set; }
            public int Right { get; set; }
            public int Bottom { get; set; }
        }
#endif

        void Update()
        {
        
            if (Input.GetKeyDown(KeyCode.Q))
            {
                Application.Quit();

            }
		    if (Input.GetKeyDown (KeyCode.K)) 
		    {
			    EnableMonkey = !EnableMonkey;
		    }
		    if (EnableMonkey) 
		    {
			    TouchList.Clear ();
			    for (int i = 0; i < NumPlayer; i++) 
			    {
				    Vector2 point = new Vector2 (UnityEngine.Random.Range(0,width),UnityEngine.Random.Range(0,height));

				    Ray ray = Camera.main.ScreenPointToRay(point);
				    RaycastHit hit;

				    Data data = new Data();

				    if (Physics.Raycast(ray, out hit, 1000))
				    {
					    //Debug.Log("hit: " + hit.collider.gameObject.name);
					    data.hit = hit;
					    data.id = 0;
				    }

				    data.Position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

				    TouchList.Add(data);
			    }
			    return;
		    }

            if (TouchList.Count > 0) TouchList.Clear();
#if UNITY_EDITOR || USE_MOUSE
            if (Input.GetMouseButton(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
                RaycastHit hit;

                Data data = new Data();

                if (Physics.Raycast(ray, out hit, 1000))
                {
                    data.hit = hit;
                    data.id = 0;
                }

                data.Position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

                TouchList.Add(data);
            }
#elif !UNITY_ANDROID && !UNITY_IOS && !UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (_engineDataType == EngineDataType.PopMessage) _engineDataType = EngineDataType.LightTouchClient;
                else _engineDataType = EngineDataType.PopMessage;
            }
#if !TABLE
            if (_engineDataType == EngineDataType.LightTouchClient)
            {
                GetDataFromLightTouchClient();
            }
#endif
            //We always run pop message from now on, but inside it won't ask for interaction if it's lighttouch
            GetDataFromPopMessage();

#endif
        }

#if !UNITY_ANDROID && !UNITY_IOS
    
    private void GetDataFromPopMessage()
    {
        try
        {
            int answer = PopMesssage(ref _EyeClickMsg);
            while (answer == 0)
            {
                // If body points location
                if (_EyeClickMsg.fMessageID == 19.71062f)
                {
                    PlayerBodyLocation playerBodyLocation = new PlayerBodyLocation();
                    playerBodyLocation.PositionX = _EyeClickMsg.fData1;
                    playerBodyLocation.PositionY = _EyeClickMsg.fData2;
                    playerBodyLocation.Length = Convert.ToInt32(_EyeClickMsg.fData3.ToString().Split('.')[0]);
                    playerBodyLocation.Height = Convert.ToInt32(_EyeClickMsg.fData3.ToString().Split('.')[1]);
                    playerBodyLocation.TrackID = _EyeClickMsg.fData4;
                    PlayerDataList.Add(playerBodyLocation);
                }
                // If gestures 
                else
                {
                    if (_engineDataType == EngineDataType.PopMessage)
                    {
                        Data data = new Data();
#if TABLE
                        float x = (_EyeClickMsg.fData1 - WindowMover.TopLeft.x) * ScreenWidth / (float)Screen.width;

                        float y = (_EyeClickMsg.fData2 - WindowMover.TopLeft.y) * ScreenHeight / (float)Screen.height;
                        Ray ray = Camera.main.ScreenPointToRay(new Vector2(x * Screen.width, (1 - y) * Screen.height));
#else
                        float x = (_EyeClickMsg.fData1);

                        float y = (_EyeClickMsg.fData2);

                        Ray ray = Camera.main.ScreenPointToRay(new Vector2(x * ScreenWidth, (1 - y) * ScreenHeight));
#endif

                        RaycastHit hit;

                        if (Physics.Raycast(ray, out hit, 1000))
                        {
                            data.hit = hit;
                            data.id = _EyeClickMsg.fData4;
                        }
#if TABLE
                        data.Position = new Vector2(x * Screen.width, (1 - y) * Screen.height);
#else
                        data.Position = new Vector2(x * ScreenWidth, (1 - y) * ScreenHeight);
#endif
                        TouchList.Add(data);
                    }
                }
                answer = PopMesssage(ref _EyeClickMsg);
            }
        }
        catch (Exception e)
        {
            //DebugText_2.text = "GetDataFromPopMessage: " + e;
        }

    }
    private void GetDataFromLightTouchClient()
    {
        if (getSilhoutteData(2, sil))
        {
            for (int row = 0; row < 24; row++)
            {
                for (int col = 0; col < 32; col++)
                {
                    if (sil[row * 32 + col] != 0)
                    {
                        Data data = new Data();
                        // Finding the location of the touch inside the sil matrix
                        float xcol = ((float)1.0f / 64.0f) + (float)col / 32.0f;

                        float yrow = 1 - ((((float)1.0f / 48.0f) + (float)row / 24.0f));

                        float x = xcol;

                        float y = yrow;

                        // Getting the object/point of the touch in the game world, we use the main camera
                        Ray ray = Camera.main.ScreenPointToRay(new Vector2(x * ScreenWidth, (1 - y) * ScreenHeight));
                        RaycastHit hit;

                        if (Physics.Raycast(ray, out hit, 1000))
                        {
                            data.hit = hit;
                            data.id = 0;
                        }

                        data.Position = new Vector2(x * ScreenWidth, (1 - y) * ScreenHeight);
                        TouchList.Add(data);
                    }
                }
            }
        }
    }
#endif
#endif
        }
    }