using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using mgCommon;
using System.IO;

public class PlaneLoading : MonoBehaviour
{
    IniFile ini;
    string filePathStr;

    void Awake()
    {
      
        //DontDestroyOnLoad(gameObject);
    }

    IEnumerator Start()
    {
        string[] str = System.Environment.GetCommandLineArgs();
        if (str.Length > 1 && System.IO.File.Exists(str[1] + "\\Frogz.ini"))
        {
            ini = new IniFile(str[1] + "\\Frogz.ini");
            filePathStr = ini.readString("Gameplay", "planeLoading", "");
            if (IsImage(filePathStr))
            {
                WWW www = new WWW("file://" + str[1] + "\\" + filePathStr);
                yield return www;
         //       renderer.material.mainTexture = www.texture;
         //       renderer.enabled = true;
            }
        }
        
    
    }

    void Update()
    {

    }

    bool IsImage(string fileName)
    {
        return (fileName.ToLower().EndsWith(".png") || fileName.ToLower().EndsWith(".jpg") || fileName.ToLower().EndsWith(".jpeg") || fileName.ToLower().EndsWith(".tga"));
    }
}
