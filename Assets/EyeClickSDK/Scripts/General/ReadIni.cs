﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
#if UNITY_ANDROID
using Assets.EyeClickSDK.Scripts.interactions;
#endif
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
using System.Runtime.InteropServices;
using System.Text;
#endif
using UnityEngine;

public class ReadIni
{
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
    private string path;

    [DllImport("KERNEL32.DLL", EntryPoint = "GetPrivateProfileStringW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
    private static extern int GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, string lpReturnString, int nSize, string lpFilename);

    [DllImport("kernel32", EntryPoint = "GetPrivateProfileStringW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
    private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);


    //[DllImport("kernel32")]
    //private static extern long WritePrivateProfileString(string section,string key,string val,string filePath);
    [DllImport("KERNEL32.DLL", EntryPoint = "WritePrivateProfileStringW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
    private static extern int WritePrivateProfileString(string lpAppName, string lpKeyName, string lpString, string lpFilename);

    [DllImport("kernel32")]
    private static extern long WritePrivateProfileSection(string section,
        string keysAndValues, string filePath);



    [DllImport("kernel32")]
    private static extern int GetPrivateProfileSection(string section,
        IntPtr keysAndValues, int bufferSize, string filePath);




    public static string GetPath()
    {
        string path = "";
        DirectoryInfo dir = new DirectoryInfo(".");
        string dirName = dir.Name;
        path = Directory.GetCurrentDirectory() + @"/" + dirName;
        return path;
    }

    /// <summary>
    /// Load Ini with full custom Path
    /// </summary>
    /// <param name="iniPath"></param>
    public ReadIni(string iniPath)
    {
        path = iniPath;
        System.IO.FileInfo fileInfo = new System.IO.FileInfo(path);
        if (!fileInfo.Exists)
        {
            FileStream fs = fileInfo.OpenWrite();
            fs.Close();
        }
        // Add full path if needed
        if (path.LastIndexOf("\\") < 0 && path.LastIndexOf("/") < 0) 
            path = fileInfo.DirectoryName + '\\' + path;
    }

    /// <summary>
    /// INIFile Constructor.
    /// </summary>
    public ReadIni()
    {
        DirectoryInfo dir = new DirectoryInfo(".");
        string dirName = dir.Name;
        path = GetPath()+ @"/" + dirName + ".ini";
        System.IO.FileInfo fileInfo = new System.IO.FileInfo(path);
        if (!fileInfo.Exists)
        {
            FileStream fs = fileInfo.OpenWrite();
            fs.Close();
        }
        // Add full path if needed
        if (path.LastIndexOf("\\") < 0 && path.LastIndexOf("/") < 0)
            path = fileInfo.DirectoryName + '\\' + path;
    }
    /// <summary>
    /// Write Data to the INI File
    /// </summary>
    /// <PARAM name="Section"></PARAM>
    /// Section name
    /// <PARAM name="Key"></PARAM>
    /// Key Name
    /// <PARAM name="Value"></PARAM>
    /// Value Name
    public void writeValue(string Section, string Key, string Value)
    {
        WritePrivateProfileString(Section, Key, Value, this.path);
    }

    public void writeString(string Section, string Key, string Value)
    {
        writeValue(Section, Key, Value);
    }

    public void writeInt(string Section, string Key, int Value)
    {
        writeValue(Section, Key, Value.ToString());
    }

    public void writeLong(string Section, string Key, long Value)
    {
        writeValue(Section, Key, Value.ToString());
    }

    public void writeBool(string Section, string Key, bool Value)
    {
        writeValue(Section, Key, Value.ToString());
    }

    /// <summary>
    /// Read Data Value From the Ini File
    /// </summary>
    /// <PARAM name="Section"></PARAM>
    /// <PARAM name="Key"></PARAM>
    /// <PARAM name="Path"></PARAM>
    /// <returns></returns>
    public string readValue(string Section, string Key, string defaultStr)
    {
        StringBuilder temp = new StringBuilder(255);
        GetPrivateProfileString(Section, Key, defaultStr, temp,
            255, this.path);
        return temp.ToString();

    }
    public string readString(string Section, string Key, string defaultStr)
    {
        return readValue(Section, Key, defaultStr);
    }
    public int readInt(string Section, string Key, int defaultVal)
    {

        string resStr = readValue(Section, Key, defaultVal.ToString());
        int res = defaultVal;
        try
        {
            res = int.Parse(resStr);
        }
        catch (System.Exception)
        {
        }
        return res;
    }
    public int[] readIntArray(string Section, string Key, int[] defaultVal)
    {
        string resStr = readValue(Section, Key, "");
        string[] list = resStr.Split(',');
        int[] res = defaultVal;

        if (resStr != null && resStr != "")
        {
            res = new int[list.Length];
            for (int i = 0; i < list.Length; i++)
            {
                int.TryParse(list[i], out res[i]);
            }

        }

        return res;
    }
    public long readLong(string Section, string Key, long defaultVal)
    {

        string resStr = readValue(Section, Key, defaultVal.ToString());
        long res = defaultVal;
        try
        {
            res = long.Parse(resStr);
        }
        catch (System.Exception)
        {
        }
        return res;
    }
    public bool readBool(string Section, string Key, bool defaultVal)
    {
        string resStr = readValue(Section, Key, defaultVal.ToString());
        bool res = defaultVal;
        try
        {
            res = bool.Parse(resStr);
        }
        catch (System.Exception)
        {
        }
        return res;
    }
    public float readFloat(string Section, string Key, float defaultVal)
    {
        string resStr = readValue(Section, Key, defaultVal.ToString());
        float res = defaultVal;
        Single.TryParse(resStr, out res);
        return res;
    }
    public float[] readFloatArray(string Section, string Key, float[] defaultVal)
    {
        string resStr = readValue(Section, Key, "");
        string[] list = resStr.Split(',');
        float[] res = defaultVal;

        if (resStr != null && resStr != "")
        {
            res = new float[list.Length];
            for (int i = 0; i < list.Length; i++)
            {
                float.TryParse(list[i], out res[i]);
            }

        }

        return res;
    }


    /// <summary>
    /// Reads a whole section of the INI file.
    /// </summary>
    /// <param name="section">Section to read.</param>
    /*public string[] readSection(string section)
    {
        const int bufferSize = 2048;

        StringBuilder returnedString = new StringBuilder(bufferSize);

        GetPrivateProfileSection(section, returnedString, bufferSize, this.path);

        string sectionData = returnedString.ToString();
        return sectionData.Split('\0');
    }*/

    //TODO: remove this function and to use the functions: GetCategories, GetKeys
    public string[] readSection(string section)
    {
        const int bufferSize = 2048;

        StringBuilder returnedString = new StringBuilder();

        IntPtr pReturnedString = Marshal.AllocCoTaskMem(bufferSize);
        try
        {
            int bytesReturned = GetPrivateProfileSection(section, pReturnedString, bufferSize, this.path);

            //bytesReturned -1 to remove trailing \0
            for (int i = 0; i < bytesReturned - 1; i++)
                returnedString.Append((char)Marshal.ReadByte(new IntPtr((uint)pReturnedString + (uint)i)));
        }
        finally
        {
            Marshal.FreeCoTaskMem(pReturnedString);
        }

        string sectionData = returnedString.ToString();
        return sectionData.Split('\0');
    }


    /// <summary>
    /// Writes a whole section of the INI file.
    /// </summary>
    /// <param name="section">Section to read.</param>
    /// <param name="keysAndValues">Array of key=value pairs.</param>
    public void writeSection(string section, string[] keysAndValues)
    {
        StringBuilder sectionData = new StringBuilder();

        foreach (string currPair in keysAndValues)
        {
            sectionData.AppendLine(currPair);
        }

        WritePrivateProfileSection(section, sectionData.ToString(), this.path);
    }

    // <summary>
    /// Reset a section
    /// </summary>
    /// <PARAM name="Section"></PARAM>
    /// Section name      
    public void resetSection(string Section)
    {
        WritePrivateProfileSection(Section, null, this.path);
    }

    public List<string> GetCategories()
    {
        string returnString = new string(' ', 65536);
        GetPrivateProfileString(null, null, null, returnString, 65536, this.path);
        List<string> result = new List<string>(returnString.Split('\0'));
        result.RemoveRange(result.Count - 2, 2);
        return result;
    }

    public List<string> GetKeys(string category)
    {
        string returnString = new string(' ', 32768);
        GetPrivateProfileString(category, null, null, returnString, 32768, this.path);
        List<string> result = new List<string>(returnString.Split('\0'));
        result.RemoveRange(result.Count - 2, 2);
        return result;
    }

    public string[] readStringArray(string Section, string Key)
    {
        string resStr = readValue(Section, Key, "");
        string[] list = resStr.Split(',');
        string[] res = null;

        if (resStr != null && resStr != "")
        {
            res = new string[list.Length];
            for (int i = 0; i < list.Length; i++)
            {
                Debug.Log(list[i]);
                res[i] = list[i];
            }

        }

        return res;
    }
#elif UNITY_ANDROID
    public Dictionary<string, Dictionary<string, string>> IniDictionary = new Dictionary<string, Dictionary<string, string>>();
    private string path;
    private bool Initialized = false;

    /// <summary>
    /// INIFile Constructor.
    /// </summary>
    public ReadIni()
    {
        string externalResourcesFolderPath = GetPath();
        var splitString = externalResourcesFolderPath.Split('/');
        Debug.Log("ResourcesFolderPath: " + externalResourcesFolderPath);
        var gameName = splitString[splitString.Length - 1];
        Debug.Log("gameName: " + gameName);
        Debug.Log("iniFile: " + externalResourcesFolderPath + "/" + gameName + ".ini");
        LoadIni(externalResourcesFolderPath + "/" + gameName + ".ini");
    }

    /// <summary>
    /// Load Ini with full custom Path
    /// </summary>
    /// <param name="iniPath"></param>
    public ReadIni(string iniPath)
    {
        LoadIni(iniPath);
    }

    public void LoadIni(string pathINI)
    {
        path = pathINI;
        FirstRead();
    }

    public static string GetPath()
    {
        return AndroidEngine.GetExternalResourcesFolderPath();
    }

    private bool FirstRead()
    {
        if (File.Exists(path))
        {
            using (StreamReader sr = new StreamReader(path))
            {
                string line;
                string theSection = "";
                string theKey = "";
                string theValue = "";
                int lineNum = 0;
                while (!sr.EndOfStream)
                {
                    line = sr.ReadLine();
                    lineNum++;
                    if (!string.IsNullOrEmpty(line))
                    {
                        line = line.Trim();
                        if (line.StartsWith("[") && line.EndsWith("]"))
                        {
                            theSection = line.Substring(1, line.Length - 2);
                            theKey = "";
                            theValue = "";
                            //Debug.Log("Section: " + theSection);
                        }
                        else
                        {
                            try
                            {
                                string[] ln = line.Split(new char[] { '=' });
                                theKey = ln[0].Trim();
                                theValue = ln[1].Trim();
                                if (ln.Length >= 3)
                                {
                                    ErrorCollector.AddMessage(ErrorMessageType.Error_INI, "Split char = got more than 2 part at line " + lineNum);
                                }
                            }
                            catch (Exception e)
                            {
                                ErrorCollector.AddMessage(ErrorMessageType.Error_INI, "Read error: " + line + ", Line: " + lineNum);
                            }

                            //UnityEngine.Debug.Log("Key: " + theKey + ", Value: " + theValue);
                        }
                        if (theSection == "" || theKey == "" || theValue == "")
                            continue;
                        PopulateIni(theSection, theKey, theValue);
                        //Debug.Log("Added");
                    }
                }
                Initialized = true;
            }

        }
        else
        {
            ErrorCollector.AddMessage(ErrorMessageType.Error_INI, "File not exist!");
        }

        return true;
    }

    private void PopulateIni(string _Section, string _Key, string _Value)
    {
        _Section = _Section.ToLower();
        _Key = _Key.ToLower();
        _Value = _Value.ToLower();
        if (IniDictionary.Keys.Contains(_Section))
        {
            if (IniDictionary[_Section].Keys.Contains(_Key))
                IniDictionary[_Section][_Key] = _Value;
            else
                IniDictionary[_Section].Add(_Key, _Value);
        }
        else
        {
            Dictionary<string, string> neuVal = new Dictionary<string, string>();
            neuVal.Add(_Key.ToString(), _Value);
            IniDictionary.Add(_Section.ToString(), neuVal);
        }
    }

    public void writeString(string Section, string Key, string Value)
    {
        IniWriteValue(Section, Key, Value);
    }

    /// <summary>
    /// Write data to INI file. Section and Key no in enum.
    /// </summary>
    /// <param name="_Section"></param>
    /// <param name="_Key"></param>
    /// <param name="_Value"></param>
    public void IniWriteValue(string _Section, string _Key, string _Value)
    {
        if (!Initialized)
        {
            FirstRead();
        }
        /*if (ErrorCollector.HaveErrorByType(ErrorMessageType.Error_INI))
        {
            return;
        }*/
        PopulateIni(_Section, _Key, _Value);
        //write ini
        WriteIni();
    }

    private void WriteIni()
    {
        using (StreamWriter sw = new StreamWriter(path))
        {
            foreach (KeyValuePair<string, Dictionary<string, string>> sezioni in IniDictionary)
            {
                sw.WriteLine("[" + sezioni.Key + "]");
                foreach (KeyValuePair<string, string> chiave in sezioni.Value)
                {
                    // value must be in one line
                    string vale = chiave.Value;
                    vale = vale.Replace(Environment.NewLine, " ");
                    vale = vale.Replace("\r\n", " ");
                    sw.WriteLine(chiave.Key + " = " + vale);
                }
            }
        }
    }

    /// <summary>
    /// Read data from INI file. Section and Key no in enum.
    /// </summary>
    /// <param name="_Section"></param>
    /// <param name="_Key"></param>
    /// <returns></returns>
    public string IniReadValue(string _Section, string _Key, string defaultValue)
    {
        _Section = _Section.ToLower();
        _Key = _Key.ToLower();
        if (!Initialized)
            FirstRead();
        if (IniDictionary.ContainsKey(_Section))
            if (IniDictionary[_Section].ContainsKey(_Key))
            {
                //UnityEngine.Debug.Log(_Key + "------" +IniDictionary[_Section][_Key]);
                return IniDictionary[_Section][_Key];
            }

        return defaultValue;
    }


    public string readString(string section, string key, string defaultVal)
    {
        string value = IniReadValue(section, key, defaultVal);
        return value;
    }

    public int readInt(string section, string key, int defaultVal)
    {
        string value = IniReadValue(section, key, defaultVal.ToString());
        int res = defaultVal;
        try
        {
            Int32.TryParse(value, out res);
        }
        catch (Exception e)
        {
            ErrorCollector.AddMessage(ErrorMessageType.Error_INI, "Parse error: " + value + ", to Int");
        }

        return res;
    }

    public float readFloat(string section, string key, float defaultVal)
    {
        string value = IniReadValue(section, key, defaultVal.ToString());
        float res = defaultVal;
        try
        {
            Single.TryParse(value, out res);
        }
        catch (Exception e)
        {
            ErrorCollector.AddMessage(ErrorMessageType.Error_INI, "Parse error: " + value + ", to Float");
        }
        return res;
    }

    public bool readBool(string section, string key, bool defaultVal)
    {
        string value = IniReadValue(section, key, defaultVal.ToString());
        bool res = defaultVal;
        try
        {
            res = Convert.ToBoolean(value);
        }
        catch (Exception e)
        {
            ErrorCollector.AddMessage(ErrorMessageType.Error_INI, "Parse error: " + value + ", to Bool");
        }
        res = Convert.ToBoolean(value);
        return res;
    }

    public long readLong(string section, string key, long defaultVal)
    {
        string value = IniReadValue(section, key, defaultVal.ToString());
        long res = defaultVal;
        try
        {
            res = long.Parse(value);
        }
        catch (Exception e)
        {
            ErrorCollector.AddMessage(ErrorMessageType.Error_INI, "Parse error: " + value + ", to Long");
        }

        return res;
    }

    public float[] readFloatArray(string section, string key, float[] defaultVal)
    {
        string resStr = IniReadValue(section, key, "");

        string[] list = resStr.Split(IGNORE_CHAR);
        float[] res = defaultVal;
        if (resStr != null && resStr != "")
        {
            res = new float[list.Length - 2];
            for (int i = 1; i < list.Length - 1; i++)
            {
                float.TryParse(list[i], out res[i - 1]);

            }

        }
        return res;
    }



    private readonly char[] IGNORE_CHAR = new char[]
    {
        ',',
        '\"'
    };

    public int[] readIntArray(string section, string key, int[] defaultVal)
    {

        string resStr = IniReadValue(section, key, "");

        string[] list = resStr.Split(IGNORE_CHAR);
        int[] res = defaultVal;
        if (resStr != null && resStr != "")
        {
            res = new int[list.Length - 2];
            for (int i = 1; i < list.Length - 1; i++)
            {
                Int32.TryParse(list[i], out res[i - 1]);
            }
        }
        return res;
    }

    public string[] readStringArray(string Section, string Key)
    {
        string resStr = IniReadValue(Section, Key, "");
        resStr = resStr.Split('"')[1];
        string[] list = resStr.Split(',');
        string[] res = null;

        if (resStr != null && resStr != "")
        {
            res = new string[list.Length];
            for (int i = 0; i < list.Length; i++)
            {
                res[i] = list[i];
            }

        }

        return res;
    }
#endif
}
