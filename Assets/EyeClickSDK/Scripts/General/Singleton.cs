﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _theInstance;

    public static T Instance
    {
        get
        {
            if (_theInstance == null)
            {
                _theInstance = (T)FindObjectOfType(typeof(T));
                if (_theInstance == null)
                {
                    GameObject singleton = new GameObject();
                    _theInstance = singleton.AddComponent<T>();
                    singleton.name = "(singleton) " + typeof(T).ToString();
                }
                DontDestroyOnLoad(FindObjectOfType(typeof(T)));
            }
            return _theInstance;
        }
    }
}