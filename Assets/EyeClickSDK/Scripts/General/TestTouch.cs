﻿using System.Collections;
using System.Collections.Generic;
using Assets.EyeClickSDK.Scripts.interactions;
using UnityEngine;

public class TestTouch : MonoBehaviour
{
    public GameObject TestTouchPrefab;
    public int MaxTouchTest = 20;
    private List<TestTouchData> touchDebugList = new List<TestTouchData>();

    private bool IsDebug;

	// Use this for initialization
	void Start ()
    {
	    for (int i = 0; i < MaxTouchTest; i++)
	    {
            touchDebugList.Add(Instantiate(TestTouchPrefab, new Vector3(1000, 1000, 1000), Quaternion.identity).GetComponent<TestTouchData>());
        }
	}
	
	// Update is called once per frame
	void Update ()
	{
        if (Input.GetKeyDown(KeyCode.D))
        {
            IsDebug = !IsDebug;
            if (!IsDebug)
            {
                for (int i = 0; i < touchDebugList.Count; i++)
                {
                    touchDebugList[i].transform.position = new Vector3(1000, 1000, 0);
                }
            }
        }
	    if (!IsDebug)
	    {
            return;
	    }
	    int index = 0;
	    for (int i = 0; i < touchDebugList.Count; i++)
	    {
	        touchDebugList[i].transform.position = new Vector3(1000, 1000, 0);
	    }
        foreach (var item in Interaction.Instance.TouchList)
        {
            
            touchDebugList[index].transform.position = new Vector3(item.hit.point.x, item.hit.point.y, item.hit.point.z);
            touchDebugList[index].Text.text = item.id.ToString();
            index++;
            if (index >= touchDebugList.Count)
            {
                index = 0;
            }
        }
    }
}
