using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using Assets.EyeClickSDK.Scripts.interactions;
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
using mgCommon;
#endif


public class VersionNumberTemplate : VersionNumber
{
    public string TemplateName = "";
    void Start()
    {
        WriteToIni();
    }

    public override void WriteToIni()
    {
        string externalResourcesFolderPath = ReadIni.GetPath() + "/";
        string path = externalResourcesFolderPath + TemplateName+".ini";
        ReadIni ini = new ReadIni(path);
        ini.writeString("Config", "gameVersion", Version);
    }
}