using System;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;
using mgCommon;
using Microsoft.Win32;
using System.Collections.Generic;
using UnityEngine.Analytics;

/// <summary>
/// Version 1.3
/// Added: Changing the size of the window
/// </summary>

public class WindowMover : MonoBehaviour
{
    public enum Table
    {
        Rectangle_4_3 = 1,
        Rectangle_16_9 = 2,
        Rectangle_1_1 = 3,
        Round = 4
    }

    public static Table _Table = Table.Rectangle_1_1;

    public static bool IsRound = false;

	static ReadIni iniGuiFrame;

    public static Vector2 TopLeft;

    public static float Height;

    public static RuntimePlatform curFlatform;
    private String androidPath = "/sdcard/Eyeclick/application.ini";
    private String pcPath = "..\\Global\\applications.ini";
    void Awake()
    {
#if UNITY_ANDROID
#if !UNITY_EDITOR
            Cursor.visible = false;
#endif
        try
        {

			iniGuiFrame = new ReadIni(androidPath);
			_Table = (Table)(iniGuiFrame.readInt("application", "Aspect", 1));
            TopLeft = new Vector2(iniGuiFrame.readInt("application", "OffsetX", 0), iniGuiFrame.readInt("application", "OffsetY", 0));
            Debug.Log("*-*-*-*-*-*-*-*" + TopLeft);
        }
        catch
        {
            _Table = WindowMover.Table.Rectangle_4_3;
        }
#elif UNITY_STANDALONE_WIN
        try
        {
            iniGuiFrame = new ReadIni(pcPath);

            TopLeft.x = iniGuiFrame.readFloat("Application", "pAR1x", 0);

            TopLeft.y = iniGuiFrame.readFloat("Application", "pAR1y", 0);

            IsRound = iniGuiFrame.readBool("Application", "RoundMask", false);

            if (IsRound)
            {
                _Table = Table.Round;
            }
            else
            {
                _Table = (Table)(iniGuiFrame.readInt("Application", "Aspect", 1) + 1);
            }

        }
        catch
        {
            Debug.Log(_Table);
            _Table = WindowMover.Table.Rectangle_4_3;
        }
#endif
        Debug.Log (_Table);
    }
    void Start()
    {
        StartGame();
    }

    void StartGame()
    {
        //Debug.Log("load scene: " + (int)WindowMover._Table);
        Application.LoadLevel( (int)WindowMover._Table);
        //Application.LoadLevel(4);
    }

    public static bool isMobile()
    {
#if MOBILE
        return true;
#endif
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            return true;
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            return true;
        }

        return false;
    }
    public static bool isPC()
    {
#if MOBILE
        return false;
#endif
        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
        {
            return true;
        }
        return false;
    }
}