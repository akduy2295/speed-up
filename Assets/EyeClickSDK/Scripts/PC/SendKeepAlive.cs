﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class SendKeepAlive : MonoBehaviour
{
#if UNITY_STANDALONE_WIN
    [DllImport("LightTouchClient.dll")]
    public static extern void sendKeepAlive();
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        try
        {
            sendKeepAlive();
        }
        catch
        {
        }
    }
#endif
}
