
public class EnumsContainer
{
    public enum TILE_SHAPE
    {
        SQUARE, CIRCLE, HEXAGON, STAR, TRIANGLE, HARRY, LOVE, BESOMS
    }

    public enum TILE_COLOR
    {
        NULL, RED, YELLOW, GREEN, BLUE, CYAN
    } 

    public enum GAME_STATE
    {
        PREPARE, PLAYING, END
    }
}