﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using static EnumsContainer;
using Random = UnityEngine.Random;
using UnityEngine.UI;
using UnityEngine.Serialization;

public class GameController : MonoBehaviour
{
    [SerializeField] Image winImagePrefab;
    // [FormerlySerializedAsAttribute("lanes")]
    [SerializeField] protected List<GameObject> lanes;
    [SerializeField] protected GameObject tilePrefab;
    [SerializeField] Image wingamePanel; //mode 3
    int completedPlayers;
    int currentPlayingPlayers;
    List<GameObject> activeTiles;
    List<KeyData> playerAnswerKeys;
    List<KeyData> keysPattern;
    int answerOrder;
    GAME_STATE gameState;
    bool isWinGame;
    List<Player> registeredPlayers;

    public static GameController instance;
    public List<GameModel> gameModels;
    List<GameModel> pattern; //used for mode 1 and mode 2

    public GAME_STATE GameState
    {
        get => gameState;
        set
        {
            switch (value)
            {
                case GAME_STATE.PREPARE:
                    break;
                case GAME_STATE.PLAYING:
                    break;
                case GAME_STATE.END:
                    if (isWinGame)
                    {
                        GameConstants.gameLevel++;
                    }
                    this.SetCallback(duration: 2, () => SceneManager.LoadScene(sceneBuildIndex: 0));
                    break;
            }
            gameState = value;
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        if (INIReader.instance == null)
        {
            SceneManager.LoadScene(0);
        }
        if (GameConstants.playerScores == null)
        {
            this.Log("init player scores list");
            GameConstants.playerScores = new List<int>() { 0, 0, 0, 0 };
        }

        activeTiles = new List<GameObject>();
        playerAnswerKeys = new List<KeyData>();
        keysPattern = new List<KeyData>();
        registeredPlayers = new List<Player>();

        this.RegisterListener(EventID.ON_GAME_STARTED, param => GameState = GAME_STATE.PLAYING);
    }

    void Start()
    {
        currentPlayingPlayers = INIReader.instance.numberOfPlayers;
        SpawnObjects();
    }

    public void TimesUp()
    {
        var count = 0;
        foreach (var player in registeredPlayers)
        {
            if (!player.IsWinGame)
            {
                player.ShowLosingEffect(() =>
                {
                    this.Log("times up: " + count);
                    if (++count == registeredPlayers.Count)
                    {
                        GameState = GAME_STATE.END;
                    }
                });
            }
        }
    }

    public List<GameObject> RequestGetTiles(int id)
    {
        if (registeredPlayers.Count == 0)
        {
            this.Log("no player has created");
            return null;
        }

        var tileList = new List<GameObject>();
        for (int i = 0; i < pattern.Count; i++)
        {
            var laneTransform = GetPlayerByID(id).transform;
            var tile = Instantiate(tilePrefab, Vector3.zero, Quaternion.identity, laneTransform);
            SetUpThisTile(ref tile, keyData: pattern[i].key, laneIndex: i);
            tileList.Add(tile);
        }
        return tileList;
    }

    //retrieve audio clip matched with the key data list generated
    public List<List<AudioClip>> RequestVoiceOver()
    {
        var res = new List<List<AudioClip>>();
        foreach (var key in keysPattern)
        {
            foreach (var model in pattern)
            {
                if (model.key == key)
                {
                    res.Add(model.sounds);
                }
            }
        }
        return res;
    }


    //request from touched tile:
    //if touched tile and the key is matched => respond a callback(true)
    //else respond a callback(false)
    public void RequestCheckTouchedTile(int playerID, KeyData data, Action<bool> callBack)
    {
        switch (INIReader.instance.gameMode)
        {
            case 1:
            case 2:
                CheckSingleAnswerRequest(playerID, data, callBack);
                break;
            case 3:
                CheckSerialAnswerRequest(data, callBack);
                break;
        }
    }

    private void CheckSerialAnswerRequest(KeyData data, Action<bool> callBack)
    {
        if (keysPattern.Contains(data)
            && keysPattern[answerOrder] == data)
        {
            this.Log("matched");
            answerOrder++;
            callBack(true);
            OnRightTileTouched(-1, data);
        }
        else
        {
            callBack(false);
        }
    }

    private void CheckSingleAnswerRequest(int playerID, KeyData data, Action<bool> callBack)
    {
        if (keysPattern.Contains(data))
        {
            this.Log("matched");
            callBack(true);
            OnRightTileTouched(playerID, data);
        }
        else
        {
            callBack(false);
        }
    }

    private IEnumerator ExplodeTiles(int index, float tick)
    {
        while (index < activeTiles.Count)
        {
            activeTiles[index++].GetComponent<Tile>().UnActivate();
            yield return new WaitForSeconds(tick);
        }
        GameState = GAME_STATE.END;
    }

    public void RequestShowTiles()
    {
        StartCoroutine(ShowAllTilesOfGrid(0));
    }

    private IEnumerator ShowAllTilesOfGrid(int index)
    {
        while (index < activeTiles.Count)
        {
            activeTiles[index++].GetComponent<Tile>().Show();
            yield return new WaitForSeconds(.1f);
        }
        TimerBoard.instance.Show();
    }

    private void OnRightTileTouched(int playerID, KeyData data)
    {
        var player = default(Player);
        switch (INIReader.instance.gameMode)
        {
            case 1:
                //get player and show effect
                player = GetPlayerByID(playerID);
                player.ShowWinningEffect(data);
                player.DisableAllTiles();

                isWinGame = true;
                GameState = GAME_STATE.END;
                break;
            case 2:
                //get player and show effect
                player = GetPlayerByID(playerID);
                player.ShowWinningEffect(data);
                player.DisableAllTiles();

                //update score of players and check if has playing player or not
                player.UpdateScore(GetScoreByRanked(GetRankOfPlayers()), data);
                if (RemovePlayerFromList(player))
                {
                    isWinGame = true;
                    GameState = GAME_STATE.END;
                }

                break;
            case 3:
                //todo: complete stage
                playerAnswerKeys.Add(data);
                if (playerAnswerKeys.SequenceEqual(keysPattern))
                {
                    isWinGame = true;
                    GameState = GAME_STATE.END;

                    //todo: split to a new class
                    wingamePanel.DOFade(0.5f, 0.5f).OnComplete(() =>
                    {
                        //show keys images
                        var parent = wingamePanel.transform.GetChild(0);
                        for (int i = 0; i < playerAnswerKeys.Count; i++)
                        {
                            var img = Instantiate<Image>(winImagePrefab, Vector3.zero, Quaternion.identity, parent);
                            img.sprite = playerAnswerKeys[i].icon;
                            img.rectTransform.sizeDelta = new Vector2(250, 250);
                        }
                    });
                }
                break;
        }
    }

    public void RequestPlayerWrongTouched(int playerID)
    {
        this.Log("unmatched");
        var player = default(Player);
        switch (INIReader.instance.gameMode)
        {
            //in case 1 and 2, a wrong touch will result in elimination
            case 1:
            case 2:
            default:
                player = GetPlayerByID(playerID);
                player.ShowLosingEffect();
                player.DisableAllTiles();

                if (RemovePlayerFromList(player))
                {
                    GameState = GAME_STATE.END;
                }

                break;
            case 3:
                StartCoroutine(ExplodeTiles(index: 0, tick: 0.1f));
                break;
        }
    }

    //remove the player and return true if there is no player is playing
    private bool RemovePlayerFromList(Player player)
    {
        registeredPlayers.Remove(player);
        return registeredPlayers.Count == 0;
    }

    private Player GetPlayerByID(int id)
    {
        foreach (var player in registeredPlayers)
        {
            if (player.Id == id)
            {
                return player;
            }
        }
        return null;
    }

    private int GetScoreByRanked(int value)
    {
        switch (value)
        {
            case 1:
                return 10;
            case 2:
                return 5;
            case 3:
                return 3;
            default:
                return 1;
        }
    }

    private int GetRankOfPlayers()
    {
        completedPlayers++;
        var ranked = default(int);
        if (completedPlayers == 1)
        {
            ranked = 1;
        }
        if (completedPlayers == 2)
        {
            ranked = 2;
        }
        if (completedPlayers == 3)
        {
            ranked = 3;
        }
        if (completedPlayers == 4)
        {
            ranked = 4;
        }
        return ranked;
    }

    private void SpawnObjects()
    {
        pattern = ParseStringPatternToGameModel(INIReader.instance.GetPattern());
        CreateKeysPattern(pattern);

        switch (INIReader.instance.gameMode)
        {
            case 1:
            case 2:
                // CreateTilesByLanes(pattern);
                break;
            case 3:
                CreateTilesByGrid(pattern);
                break;
        }
    }

    private void CreateKeysPattern(List<GameModel> patt)
    {
        var keyList = INIReader.instance.GetKeysList().Split(',');

        for (int i = 0; i < keyList.Length; i++)
        {
            for (int k = 0; k < patt.Count; k++)
            {
                //find the matched keydata corresponding to the key string
                keyList[i] = keyList[i].Trim().ToLower().Replace(" ", "");

                if (patt[k].key.name == keyList[i])
                {
                    keysPattern.Add(patt[k].key);
                    break;
                }
            }
        }
    }

    private List<GameModel> ParseStringPatternToGameModel(string pattern)
    {
        //nomralize
        pattern = NormalizeMyString(pattern);

        //split
        var splitArray = pattern.Split(',');

        //Create Models
        var models = new List<GameModel>();

        for (int i = 0; i < splitArray.Length; i++)
        {
            models.Add(GetModelByPattern(splitArray[i]));
        }



        return models;
    }

    public string NormalizeMyString(string str)
    {
        return str.ToLower().Replace(" ", "").Trim();
    }

    private void CreateTilesByGrid(List<GameModel> models)
    {
        var totalTiles = lanes.Count * INIReader.instance.tilesEachLane;

        //init key icons
        var spriteList = new List<Sprite>();
        foreach (var mdl in models)
        {
            spriteList.Add(mdl.key.icon);
        }

        //init key tiles
        var tileList = new List<GameObject>();
        for (int i = 0; i < models.Count; i++)
        {
            var tile = Instantiate(tilePrefab, Vector3.zero, Quaternion.identity);
            tile.GetComponent<Tile>().Init(models[i].key);
            tile.GetComponent<Tile>().Hide();
            activeTiles.Add(tile);
            tileList.Add(tile);
            totalTiles--;
        }

        ///init other tiles
        var otherModels = new List<GameModel>(gameModels);

        //remove initialized models
        for (int i = 0; i < models.Count; i++)
        {
            otherModels.Remove(models[i]);
        }

        //random in the rest tiles and create
        for (int i = 0; i < totalTiles; i++)
        {
            var mdl = otherModels[Random.Range(0, otherModels.Count)];
            var tile = Instantiate(tilePrefab, Vector3.zero, Quaternion.identity);
            tile.GetComponent<Tile>().Init(mdl.key);
            tile.GetComponent<Tile>().Hide();
            activeTiles.Add(tile);
            tileList.Add(tile);
        }

        //locate all initialized tiles to lanes
        for (int i = 0; i < lanes.Count; i++)
        {
            for (int k = 0; k < INIReader.instance.tilesEachLane; k++)
            {
                var tile = tileList[Random.Range(0, tileList.Count)];
                tile.transform.SetParent(lanes[i].transform);
                tileList.Remove(tile);
            }
        }
    }

    private GameModel GetModelByPattern(string ordered)
    {
        foreach (var model in gameModels)
        {
            if (model.key.name == ordered)
            {
                return model;
            }
        }
        return default(GameModel);
    }

    private void SetUpThisTile(ref GameObject tile, KeyData keyData, int laneIndex)
    {
        var tileComponent = tile.GetComponent<Tile>();
        tileComponent.Init(keyData);
        tileComponent.Hide();
    }

    public void RequestRegisterPlayer(Player player)
    {
        registeredPlayers.Add(player);
    }
}
