using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct GameModel
{
    public List<AudioClip> sounds;
    public KeyData key;
    public static bool IsNull;
    public static void Null()
    {
        IsNull = true;
    }
}