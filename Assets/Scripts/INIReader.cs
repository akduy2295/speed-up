﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class INIReader : MonoBehaviour
{
    public static INIReader instance;
    public int numberOfPlayers;
    public int gameMode;
    public int tilesEachLane;
    public float gameDuration;
    public float wrongTouchDelay;

    ReadIni reader;
    List<string> patternList;
    List<string> keyList;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        patternList = new List<string>();
    }

    private void Start()
    {
        reader = new ReadIni();
        LoadData();
    }

    private void LoadData()
    {
        gameMode = reader.readInt("General", "mode", 1);
        numberOfPlayers = reader.readInt("General", "numberOfPlayers", 2);
        gameDuration = reader.readFloat("General", "gameDuration", 10);
        tilesEachLane = reader.readInt("General", "tilesEachLane", 5);
        wrongTouchDelay = reader.readFloat("General", "wrongTouchDelay", 0.25f);
        CreatePatternList();
    }

    private void CreatePatternList()
    {
        var sectionName = default(string);
        switch (gameMode)
        {
            case 1:
                sectionName = "mode1";
                break;
            case 2:
                sectionName = "mode2";
                break;
            case 3:
                sectionName = "mode3";
                break;
        }
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        var sectionKeys = reader.GetKeys(sectionName);
        var dataArray = new List<string>();
        var keyArray = new List<string>();

        foreach (var key in sectionKeys)
        {
            if (key.Contains("data"))
            {
                dataArray.Add(reader.readString(sectionName, key, ""));
            }
            if (key.Contains("key"))
            {
                keyArray.Add(reader.readString(sectionName, key, ""));
            }
        }
#elif UNITY_ANDROID
        var sectionKeys = reader.IniDictionary[sectionName];
        var dataArray = new List<string>();
        var keyArray = new List<string>();

        foreach (var key in sectionKeys.Keys)
        {
            if (key.Contains("data"))
            {
                dataArray.Add(reader.readString(sectionName, key, ""));
            }
            if (key.Contains("key"))
            {
                keyArray.Add(reader.readString(sectionName, key, ""));
            }
        }
#endif
        patternList = new List<string>(dataArray);
        keyList = new List<string>(keyArray);
    }

    public string GetPattern()
    {
        var gameLevel = ClampGameLevel(patternList.Count - 1);
        this.Log("gamelevel = " + gameLevel);
        this.Log("pattern list count = " + patternList.Count);
        return patternList[gameLevel];
    }

    public string GetKeysList()
    {
        var gameLevel = ClampGameLevel(keyList.Count - 1);
        return keyList[gameLevel];
    }

    private int ClampGameLevel(int max)
    {
        return Mathf.Clamp(GameConstants.gameLevel, 0, max);
    }

}
