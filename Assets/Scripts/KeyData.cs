using UnityEngine;

[System.Serializable]
public struct KeyData
{
    public string name;
    public Sprite icon;

    public static bool operator ==(KeyData a, KeyData b)
    {
        return (a.name == b.name);
    }

    public static bool operator !=(KeyData a, KeyData b)
    {
        return !(a == b);
    }
}