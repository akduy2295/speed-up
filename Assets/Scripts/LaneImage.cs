﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class LaneImage : MonoBehaviour
{
    public bool isCompleted;

    [SerializeField] Color honorColor, eliminatedColor;
    [SerializeField] Text text;
    string honorText = "BRILLIANT!!!!", eliminatedText = "IDIOT!!!!";
    Image image;
    bool isHonored;

    private void Awake()
    {
        image = GetComponent<Image>();
    }

    void Start()
    {
        image.fillAmount = 0;
    }

    public void GotEliminated(Action onComplete = null)
    {
        if (!isHonored)
        {
            image.color = eliminatedColor;
            text.text = eliminatedText;
            image.DOFillAmount(1, .75f).OnComplete(() =>
            {
                if (onComplete != null)
                {
                    onComplete();
                }
            });
        }
    }

    public void GotHonored(Action onComplete = null)
    {
        isHonored = true;
        image.color = honorColor;
        text.text = honorText;
        image.DOFillAmount(1, .75f).OnComplete(() =>
        {
            if (onComplete != null)
            {
                onComplete();
            }
        });
    }
}
