﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour
{
    public static void LoadScene()
    {
        switch (INIReader.instance.gameMode)
        {
            case 1:
                SceneManager.LoadScene("Gameplay_1");
                break;
            case 2:
                SceneManager.LoadScene("Gameplay_2");
                break;
            case 3:
                SceneManager.LoadScene("Gameplay_3");
                break;
        }
    }
    void Start()
    {
        LoadScene();
    }
}
