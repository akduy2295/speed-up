public enum EventID
{
    NONE = 0,
    ON_GAME_STARTED,
    ON_RIGHT_TILE_TOUCHED
}