﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Player : MonoBehaviour
{
    [SerializeField] GameObject imagePrefab;
    [SerializeField] GameObject tileFragmentsContainer;
    [SerializeField] GameObject scoreText;
    [SerializeField] int id;
    List<GameObject> myTiles;
    int score;
    bool isWinGame;

    public int Id { get => id; }
    public bool IsWinGame { get => isWinGame; set => isWinGame = value; }

    private void RequestGameControllerToGetTiles()
    {
        myTiles = GameController.instance.RequestGetTiles(Id);
    }

    private void RequestGameControllerToRegister()
    {
        GameController.instance.RequestRegisterPlayer(this);
    }

    public void OnStomperHasStomped()
    {
        RequestGameControllerToRegister();
        RequestGameControllerToGetTiles();
        RegisterIDForTiles();
        UpdateScoreTextValues();
        ShowTilesAppearAnimation();
    }

    private void UpdateScoreTextValues()
    {
        if (INIReader.instance.gameMode == 2)
        {
            score = GameConstants.playerScores[id];
            scoreText.GetComponent<Text>().text = score.ToString();
        }
    }

    private void RegisterIDForTiles()
    {
        foreach (var item in myTiles)
        {
            item.GetComponent<Tile>().RegisterIDFromPlayer(this.Id);
        }
    }

    private void ShowTilesAppearAnimation()
    {
        StartCoroutine(ShowTiles(count: 0, tick: 0.1f));
    }

    private IEnumerator ShowTiles(int count, float tick)
    {
        while (count < myTiles.Count)
        {
            myTiles[count++].GetComponent<Tile>().Show();
            yield return new WaitForSeconds(tick);
        }
        TimerBoard.instance.Show();
    }

    public void ShowWinningEffect(KeyData data)
    {
        IsWinGame = true;
        foreach (var tile in myTiles)
        {
            var tileComponent = tile.GetComponent<Tile>();
            if (tileComponent.Data != data)
            {
                tileComponent.Fade();
            }
        }
    }

    public void UpdateScore(int score, KeyData data)
    {
        this.score += score;
        PlayScoreEffect(score, data);
        SaveScore();
    }

    private void SaveScore()
    {
        this.Log("save " + score + " to list");
        GameConstants.playerScores[id] = score;
    }

    private void PlayScoreEffect(int score, KeyData data)
    {
        var position = GetTilePositionByKey(data);

        //create tile fragments
        var tileFragments = new List<GameObject>();
        for (int i = 0; i < score; i++)
        {
            var fragment = Instantiate(imagePrefab, position, Quaternion.identity, tileFragmentsContainer.transform);
            fragment.name = "fragment";
            fragment.GetComponent<Image>().sprite = data.icon;
            fragment.GetComponent<RectTransform>().sizeDelta = new Vector2(30, 30);
            tileFragments.Add(fragment);
        }

        //move the fragments to the score text position
        StartCoroutine(MoveTheFragmentsToTheDestination(0, tileFragments));
    }

    private IEnumerator MoveTheFragmentsToTheDestination(int index, List<GameObject> tileFragments)
    {
        var number = GameConstants.playerScores[id];
        while (index < tileFragments.Count)
        {
            var current = tileFragments[index];
            current.transform.DOMoveX(-7f, .5f)
            .OnUpdate(() =>
            {
                current.transform.position = new Vector2(current.transform.position.x, current.transform.position.y + Random.Range(-.15f, .15f));
            })
            .OnComplete(() =>
            {
                //hide the fragments
                current.SetActive(false);

                //animate the score text
                if (!DOTween.IsTweening(scoreText.transform))
                {
                    scoreText.transform.DOPunchScale(Vector2.one * 1.05f, .5f, 5);
                }
                else
                {
                    DOTween.Restart(scoreText.transform);
                }

                //update score text
                number++;
                scoreText.GetComponent<Text>().text = (number).ToString();

                this.Log("tile complete Do move x");
            });
            index++;
            yield return new WaitForSeconds(.125f);
        }
    }

    private Vector3 GetTilePositionByKey(KeyData data)
    {
        foreach (var item in myTiles)
        {
            if (item.GetComponent<Tile>().Data == data)
            {
                return item.transform.position;
            }
        }
        return default(Vector3);
    }

    public void ShowLosingEffect()
    {
        StartCoroutine(ShowLosingTileAnimation(0));
    }

    public void ShowLosingEffect(Action callback)
    {
        StartCoroutine(ShowLosingTileAnimation(0, callback));
    }

    private IEnumerator ShowLosingTileAnimation(int index)
    {
        while (index < myTiles.Count)
        {
            var tileComponent = myTiles[index].GetComponent<Tile>();
            tileComponent.UnActivate();
            index++;
            yield return new WaitForSeconds(0.2f);
        }
    }

    private IEnumerator ShowLosingTileAnimation(int index, Action callback)
    {
        while (index < myTiles.Count)
        {
            var tileComponent = myTiles[index].GetComponent<Tile>();
            tileComponent.UnActivate();
            index++;
            yield return new WaitForSeconds(0.2f);
        }
        callback();
    }

    public void DisableAllTiles()
    {
        foreach (var tile in myTiles)
        {
            tile.GetComponent<Collider>().enabled = false;
        }
    }
}
