using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class QuestionsAnnouncer : MonoBehaviour
{
    public static QuestionsAnnouncer instance;
    AudioSource audioSource;
    List<AudioClip> audioList;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        this.RegisterListener(EventID.ON_GAME_STARTED, param => AnnounceQuestion());

        audioSource = GetComponent<AudioSource>();
    }

    private void AnnounceQuestion()
    {
        var respondFromController = GameController.instance.RequestVoiceOver();
        audioList = ConvertRespondToList(respondFromController);
        StartCoroutine(PlaySound(soundIndex: 0));
    }

    private IEnumerator PlaySound(int soundIndex)
    {
        while (soundIndex < audioList.Count)
        {
            var soundDuration = audioList[soundIndex].length;
            audioSource.PlayOneShot(audioList[soundIndex]);
            soundIndex++;
            yield return new WaitForSeconds(soundDuration);
        }
    }

    private List<AudioClip> ConvertRespondToList(List<List<AudioClip>> respondFromController)
    {
        var res = new List<AudioClip>();
        foreach (var list in respondFromController)
        {
            foreach (var sound in list)
            {
                res.Add(sound);
            }
        }
        return res;
    }
}