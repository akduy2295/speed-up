﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreText : MonoBehaviour
{
    [SerializeField] int laneID;
    Text text;
    int score;

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    void Start()
    {
        this.RegisterListener(EventID.ON_GAME_STARTED, param => Show());
        this.RegisterListener(EventID.ON_RIGHT_TILE_TOUCHED, param => UpdateScore(param));
    }

    private void UpdateScore(object param)
    {
        try
        {
            Tuple<int, int> data = (Tuple<int, int>)param;
            int laneID = data.Item1;
            int score = data.Item2;

            if (this.laneID == laneID)
            {
                this.score += score;
                text.text = score.ToString();
            }
        }
        catch (System.Exception)
        {
            throw;
        }
    }

    private void Show()
    {
        text.enabled = true;
    }
}
