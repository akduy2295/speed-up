using System.Collections.Generic;
using UnityEngine;

// [CreateAssetMenu(fileName = "ScriptableGameModel", menuName = "ScriptableGameModel", order = 0)]
public class ScriptableGameModel : ScriptableObject
{
    public List<NestedData> data;
    
    public class NestedData 
    {
        public List<AudioClip> sounds;
        public KeyData key;
        public static bool IsNull;
        public static void Null()
        {
            IsNull = true;
        }
    }
}