﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using Assets.EyeClickSDK.Scripts.interactions;

public class StartButton : MonoBehaviour
{
    [SerializeField] GameObject trail;
    RectTransform rectransform;
    Vector3[] corners;
    bool active;

    private void Awake()
    {
        rectransform = GetComponent<RectTransform>();
    }

    void Start()
    {
        // corners = new Vector3[4];
        // rectransform.GetWorldCorners(corners);
        // PolishCorners();
        // trail.transform.position = corners[0];
        // StartCoroutine(MoveTrail(1));
        GetComponent<Image>().DOFillAmount(0, 3f).OnComplete(() =>
        {
            GameController.instance.RequestShowTiles();
        });
    }

    private void PolishCorners()
    {
        var v3 = new Vector3(0.05f, 0.05f);
        corners[0] = new Vector3(corners[0].x + v3.x, corners[0].y + v3.y);
        corners[1] = new Vector3(corners[1].x + v3.x, corners[1].y - v3.y);
        corners[2] = new Vector3(corners[2].x - v3.x, corners[2].y - v3.y);
        corners[3] = new Vector3(corners[3].x - v3.x, corners[3].y + v3.y);
    }

    private IEnumerator MoveTrail(int i)
    {
        while (true)
        {
            trail.transform.position = Vector2.MoveTowards(trail.transform.position, corners[i], Time.deltaTime * 5f);
            if (Vector2.Distance(trail.transform.position, corners[i]) <= 0.0001f)
            {
                if (i < corners.Length - 1)
                {
                    i++;
                }
                else
                {
                    i = 0;
                }
                continue;
            }
            yield return null;
        }
    }

    void Update()
    {
        // if (!active && GameController.instance.GameState == EnumsContainer.GAME_STATE.PREPARE)
        // {
        //     foreach (var touch in Interaction.Instance.TouchList)
        //     {
        //         if (touch.hit.collider.gameObject == this.gameObject)
        //         {
        //             active = true;
        //             trail.SetActive(false);
        //             GetComponent<Image>().DOFillAmount(0, 0.5f).OnComplete(() =>
        //             {
        //                 GameController.instance.RequestShowTiles();
        //             });
        //         }
        //     }
        // }
    }
}
