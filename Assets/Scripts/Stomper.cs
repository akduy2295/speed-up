﻿using System.Collections;
using System.Collections.Generic;
using Assets.EyeClickSDK.Scripts.interactions;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Stomper : MonoBehaviour
{
    [SerializeField] Player playerReference;
    bool isActive;
    Vector3 startScale;
    Image image;

    private void Awake()
    {
        image = GetComponent<Image>();
    }

    private void Start()
    {
        startScale = transform.localScale;

        transform.DOScale(new Vector3(1.1f, 1.1f), .25f).SetLoops(-1, LoopType.Yoyo);
    }
    void Update()
    {
        if (GameController.instance.GameState != EnumsContainer.GAME_STATE.END && !isActive)
        {
            foreach (var touch in Interaction.Instance.TouchList)
            {
                if (touch.hit.collider.gameObject == this.gameObject)
                {
                    isActive = true;
                    DOTween.Kill(transform);
                    transform.localScale = startScale;
                    image.color += new Color(0, 0, 0, -1f);
                    playerReference.OnStomperHasStomped();
                    break;
                }
            }
        }
    }
}
