﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using static EnumsContainer;
using System;
using Random = UnityEngine.Random;
using Assets.EyeClickSDK.Scripts.interactions;

public class Tile : MonoBehaviour
{
    [SerializeField] Image image;
    [SerializeField] ParticleSystem particle;
    // Image tileBG;
    int interactionCount;
    int maxInteractionCount;
    bool active;
    KeyData data;
    int id;

    public KeyData Data { get => data; set => data = value; }

    private void Awake()
    {
        active = true;
        // tileBG = GetComponent<Image>();
    }

    private void Start()
    {
        RotateAutomatically();
        DoPunchAnimation();

        maxInteractionCount = (int)(60 * INIReader.instance.wrongTouchDelay);
    }

    private void Update()
    {
        if (active && GameController.instance.GameState == GAME_STATE.PLAYING)
        {
            foreach (var touch in Interaction.Instance.TouchList)
            {
                if (IsTouchedThisObject(touch.hit.collider.gameObject))
                {
                    GameController.instance.RequestCheckTouchedTile(id, this.data, rightTouched =>
                    {
                        if (!rightTouched)
                        {
                            interactionCount++;
                            if (interactionCount >= maxInteractionCount)
                            {
                                //sure false
                                active = false;
                                GameController.instance.RequestPlayerWrongTouched(id);
                            }
                        }
                        else
                        {
                            active = false;
                            DOTween.Kill(transform);
                            particle.Play();
                            transform.localScale = Vector2.one * 2;
                        }
                    });
                }
                else
                {
                    interactionCount = 0;
                }
                break;
            }
        }
    }

    public void Fade()
    {
        image.DOColor(image.color + new Color(0, 0, 0, -.75f), .25f);
    }

    public void RegisterIDFromPlayer(int id)
    {
        this.id = id;
    }

    private bool IsTouchedThisObject(GameObject o)
    {
        return o == this.gameObject;
    }

    private void DoPunchAnimation()
    {
        var duration = Random.Range(1.5f, 3f);
        transform.DOPunchScale(new Vector3(0.5f, 0.5f), duration, 2, 0).SetLoops(-1, LoopType.Restart);
    }

    private void RotateAutomatically()
    {
        var direction = Random.Range(0, 10) % 2 == 0 ? 1 : -1;
        var duration = Random.Range(1.5f, 3f);
        transform.DORotate(new Vector3(0, 0, 200 * direction), duration).SetLoops(-1, LoopType.Incremental);
    }

    public void Hide()
    {
        image.enabled = false;
        image.DOFade(0, .25f);
        GetComponent<Collider>().enabled = false;
    }

    public void Show()
    {
        image.enabled = true;
        image.DOFade(1, .25f);
        GetComponent<Collider>().enabled = true;
    }

    public void Init(KeyData data)
    {
        this.data = data;
        image.sprite = data.icon;
    }

    public void GotBlack()
    {
        image.color = Color.black;
    }

    public void UnActivate()
    {
        GotBlack();
        Fade();
        GetComponent<Collider>().enabled = false;
        DOTween.Kill(transform);
    }
}
