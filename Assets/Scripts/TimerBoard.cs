﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class TimerBoard : MonoBehaviour
{
    public static TimerBoard instance;

    [SerializeField] Text text;
    [SerializeField] Image image;
    float count = 3;
    bool isShowed;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void Show()
    {
        if (!isShowed)
        {
            isShowed = true;
            this.Log("timerboard show");
            image.DOFillAmount(1, 0.5f).OnComplete(() =>
            {
                this.PostEvent(EventID.ON_GAME_STARTED);
                StartTimer();
            });
        }
    }

    public void StartTimer()
    {
        image.DOFillAmount(0, INIReader.instance.gameDuration)
        .OnUpdate(() =>
        {
            if (GameController.instance.GameState == EnumsContainer.GAME_STATE.END)
            {
                DOTween.Kill(image);
            }
        })
        .OnComplete(() =>
        {
            text.text = "Time's up!!!";
            GameController.instance.TimesUp();
        });
    }

    // private IEnumerator CountDownToStart(float duration)
    // {
    //     text.text = (--count).ToString();
    //     yield return new WaitForSeconds(1);
    //     if (count > 0)
    //     {
    //         StartCoroutine(CountDownToStart(count));
    //     }
    //     else
    //     {
    //         GameController.instance.GameState = EnumsContainer.GAME_STATE.PLAYING;
    //         GetQuestionFromGameController();

    //     }
    // }

    // private void GetQuestionFromGameController()
    // {
    //     var txt = GameController.instance.RequestKeyPattern();
    //     text.text = txt;
    // }
}
